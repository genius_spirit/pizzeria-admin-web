import axios from '../../axios-api';

import {
  CREATE_DISH_FAILURE, CREATE_DISH_REQUEST, CREATE_DISH_SUCCESS, DELETE_DISH_SUCCESS, EDIT_DISH_SUCCESS,
  FETCH_DISHES_SUCCESS
} from "./actionTypes";


export const createDishRequest = () => {
  return {type: CREATE_DISH_REQUEST};
};

export const createDishSuccess = (dish, id) => {
  return {type: CREATE_DISH_SUCCESS, dish, id};
};

export const createDishFailure = error => {
  return {type: CREATE_DISH_FAILURE, error};
};

export const createDish = dish => {
  return dispatch => {
    dispatch(createDishRequest());

    return axios.post('/dishes.json', dish).then(
      response => dispatch(createDishSuccess(dish, response.data.name)),
      error => dispatch(createDishFailure(error))
    );
  };
};

export const fetchDishesSuccess = dishes => {
  return {type: FETCH_DISHES_SUCCESS, dishes};
};

export const fetchDishes = () => {
  return dispatch => {
    return axios.get('/dishes.json').then(
      response => dispatch(fetchDishesSuccess(response.data))
    )
  }
};

export const editDishSuccess = (dish, id) => {
  return {type: EDIT_DISH_SUCCESS, dish, id};
};

export const editDish = (id, dish) => {
  return dispatch => {
    return axios.patch('/dishes/' + id + '.json', dish).then(
      response => dispatch(editDishSuccess(dish, id))
    );
  }
};

export const deleteDishSuccess = id => {
  return {type: DELETE_DISH_SUCCESS, id};
};

export const deleteDish = id => {
  return dispatch => {
    return axios.delete('/dishes/' + id + '.json').then(
      response => dispatch(deleteDishSuccess(id))
    )
  }
};

