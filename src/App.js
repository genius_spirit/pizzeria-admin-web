import React, {Component, Fragment} from 'react';
import Toolbar from "./components/UI/Toolbar/Toolbar";
import {Route, Switch} from "react-router-dom";
import Dishes from "./containers/Dishes/Dishes";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Toolbar />
        <Switch>
          <Route path="/" exact component={Dishes} />
        </Switch>
      </Fragment>
    );
  }
}

export default App;
